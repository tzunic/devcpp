Image based on Ubuntu 16.04.

Packages included:

- nmap              7.01-2
- cmake             3.5.1
- clang             3.8.0-2
- clang-format      3.8
- clang-tidy        3.8
- python-dev        2.7.11+
- python3-dev       3.5.1+
- npm               3.5.2
- libc++-dev        3.7.0-1
- build-essential   12.1
- qemu-system-x86   2.5+dfsg-5
- git               2.7.4-0
- vim-gtk           7.4.1689-3
- exuberant-ctags   5.9~svn20110310-11
- libncurses5-dev   6.0+20160213
- libclang-dev      3.8-33
- netcat            1.10-41
- pv                1.6.0-1
- tmux              2.1-3build1
- gcc-multilib      5.3.1-1
- libgtest-dev      1.7.0-4
- google-mock       1.7.0-18092013-1
- meson             0.31
- ninja             v1.6.0
- boost             1.60.0
- default-jre       1.8-56
- graphviz          2.38.0
- plantuml.jre      version 8040
- doxygen           1.8.11-1
- automake          1.15
- libtool           2.46
- libxml2           2.9.3
- libxml2-dev       2.9.3
- quickfix          1.14.3
- wget              1.17.1
- pkg-config        0.29.1
