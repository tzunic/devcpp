FROM ubuntu:16.04

ENV BUILD_PKGS wget curl sudo unzip bash-completion nmap cmake clang clang-format clang-tidy python-dev python3-dev npm libc++-dev build-essential qemu-system-x86 git vim-gtk exuberant-ctags libncurses5-dev libclang-dev netcat pv tmux gcc-multilib libgtest-dev google-mock libbz2-dev zlib1g-dev default-jre graphviz doxygen automake libtool libxml2-dev pkg-config

ENV LANG en_US.UTF-8
ENV LANGUAGE en_US:en
ENV LC_ALL en_US.UTF-8

RUN \
    locale-gen en_US.UTF-8 && \
    apt-get update && \
    apt-get install -y $BUILD_PKGS && \
    curl -L 'https://github.com/ninja-build/ninja/releases/download/v1.6.0/ninja-linux.zip' > ninja-linux.zip && \
    unzip ninja-linux.zip && mv ninja /usr/local/bin && rm ninja-linux.zip && \
    mkdir -p ~/.vim/autoload && \
    curl -fLo ~/.vim/autoload/plug.vim https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim && \
    curl -fLo ~/.vimrc https://bitbucket.org/tzunic/devcpp/raw/master/vimrc && \
    curl -fLo ~/.ycm_extra_conf.py https://bitbucket.org/tzunic/devcpp/raw/master/ycm_extra_conf.py && \
    curl -fLo ~/.tmux.conf https://bitbucket.org/tzunic/devcpp/raw/master/tmux.conf && \
    vim +PlugInstall +qall && \
    cd ~/.vim/plugged/YouCompleteMe/ && \
    ./install.py --clang-completer --system-libclang && \
    mkdir -p /opt/vim_files && \
    mv ~/.vim ~/.vimrc ~/.ycm_extra_conf.py ~/.tmux.conf /opt/vim_files && \
    chmod -R 755 /opt/vim_files && \
    dest="/opt" && \
    curl -L https://github.com/mesonbuild/meson/releases/download/0.31.0/meson-0.31.0.tar.gz | tar xzf - -C $dest && \
    ln -sf /opt/meson-0.31.0/meson.py /usr/local/bin/meson && \
    curl -Lo $dest/plantuml.jar https://bitbucket.org/tzunic/devcpp/raw/master/plantuml.jar && \
    mkdir $dest/quickfix && curl -Lo $dest/quickfix/quickfix.zip https://github.com/quickfix/quickfix/archive/master.zip &&  \
    unzip $dest/quickfix/quickfix.zip -d $dest/quickfix && \
    cd $dest/quickfix/quickfix-master && ./bootstrap && ./configure --enable-static=yew --enable-shared=no && make && make install && \
    curl -L https://sourceforge.net/projects/boost/files/boost/1.60.0/boost_1_60_0.tar.gz/download | tar xzf - -C $dest && \
    cd $dest/boost_1_60_0/ && \
    ./bootstrap.sh && \
    ./b2 install --toolset=gcc  --address-model=64  && \
    ldconfig && \
    apt-get clean && rm -rf /opt/boost_1_60_0 $dest/quickfix /usr/lib/apt/lists/* /tmp/* /var/tmp/*
